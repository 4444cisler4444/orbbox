FROM debian:testing
LABEL maintainer="Chris I <isl.chr@proton.me>"

# Base packages
RUN apt-get update \
  && apt-get install --assume-yes --quiet --no-install-recommends git tmux \
  wget curl man ca-certificates sudo build-essential less locales \
  gnupg ssh file xclip python3-pip python3-setuptools tig w3m gawk autojump \
  fzf lilypond ranger postgresql

# Install Node.js
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
ENV NODE_MAJOR=20
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update && apt-get install nodejs -y \
  && npm install --global yarn 2>&1 \
  # smoke test
  && node --version \
  && npm --version \
  && yarn --version

# Install locales
RUN rm -rf /var/lib/apt/lists/* \
  && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

# Create passwordless non-root user account
RUN groupadd --gid 1000 devuser \
  && useradd --uid 1000 --gid devuser --shell /bin/bash --create-home devuser \
  && chgrp --recursive devuser /usr/local \
  && find /usr/local -type d | xargs chmod g+w \
  && printf "devuser ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/devuser \
  && chmod 0440 /etc/sudoers.d/devuser

# Github is a known host
# RUN mkdir ~/.ssh && ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts

# Be non-root user
# Warning: This affects all future commands!
USER devuser
ENV USER devuser
ENV HOME /home/devuser

# Install Rust, cargo, eza, ripgrep
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --profile minimal -y 2>&1 \
  && echo "source $HOME/.cargo/env" >> ~/.bashrc \
  && . $HOME/.cargo/env \
  && cargo install eza ripgrep bat 2>&1

# Move to temporary directory
WORKDIR /usr/local/src
# Bash autocompletion
RUN git clone --recursive --depth 1 --shallow-submodules https://github.com/akinomyoga/ble.sh.git 2>&1 \
  && make -C ble.sh install PREFIX=~/.local \
  && echo 'source ~/.local/share/blesh/ble.sh' >> ~/.bashrc

# Saner terminal
RUN echo 'set completion-ignore-case on' >> ~/.inputrc \
  && echo 'set show-all-if-ambiguous on' >> ~/.inputrc \
  && echo '"\e[A": history-search-backward' >> ~/.inputrc \
  && echo '"\e[B": history-search-forward' >> ~/.inputrc \
  && echo 'set match-hidden-files on' >> ~/.inputrc \
  && echo '' >> ~/.inputrc 

# Autojump
RUN echo "[ -f /usr/share/autojump/autojump.sh ] && . /usr/share/autojump/autojump.sh" >> ~/.bashrc

# Neovim editor
RUN sudo apt-get update && sudo apt-get install -y --quiet --no-install-recommends ninja-build gettext cmake unzip \
  && git clone --depth 1 https://github.com/neovim/neovim /usr/local/src/neovim 2>&1
WORKDIR /usr/local/src/neovim
RUN make CMAKE_BUILD_TYPE=RelWithDebInfo && sudo make install
WORKDIR /usr/local/src
RUN rm -rf neovim 

# Install pipenv and add pipenv install location to PATH
RUN sudo mv "/usr/lib/python3.11/EXTERNALLY-MANAGED" "/usr/lib/python3.11/EXTERNALLY-MANAGED.old" \
  && sudo pip install pipenv pynvim 2>&1
ENV PATH="/home/devuser/.local/bin:${PATH}"

# Reddit in terminal (screw user hostile browsers and websites)
RUN sudo apt-get update && sudo apt-get install -y --quiet --no-install-recommends rtv \
  pass irssi hledger python3.11-venv lua5.3 exuberant-ctags

# Terminals created inside Neovim need this to have the PATH 
RUN echo '. "$HOME/.bashrc"' >> /home/devuser/.profile

# Neovim editor plugin manager
RUN sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim' 2>&1

ENV TERM=xterm-256color \
  DEBIAN_FRONTEND=noninteractive

# Neovim editor config
RUN mkdir -pv /home/devuser/.vim/rc \
  && touch "$HOME/.vim/rc/general-settings.vim" \
  && touch "$HOME/.vim/rc/mappings.vim" \
  && touch "$HOME/.vim/rc/plugins.vim" \
  && touch "$HOME/.vim/rc/plugin-settings.lua" \
  && touch "$HOME/.vim/rc/plugin-mappings.vim" \
  && echo "source $HOME/.vim/rc/general-settings.vim" >> ~/.vimrc \
  && echo "source $HOME/.vim/rc/mappings.vim" >> ~/.vimrc \
  && echo "source $HOME/.vim/rc/plugins.vim" >> ~/.vimrc \
  && echo "source $HOME/.vim/rc/plugin-settings.lua" >> ~/.vimrc \
  && echo "source $HOME/.vim/rc/plugin-mappings.vim" >> ~/.vimrc \
  && mkdir ~/.config \
  && ln --symbolic ~/.vim ~/.config/nvim \
  && ln --symbolic ~/.vimrc ~/.config/nvim/init.vim  \
  # General Settings
  && echo "set signcolumn=yes updatetime=300 mouse=a textwidth=99 timeoutlen=350 shell=bash" >> ~/.vim/rc/general-settings.vim \
  && echo "set pumheight=12" >> ~/.vim/rc/general-settings.vim \
  && echo "set number nocursorline relativenumber nowrap splitbelow splitright" >> ~/.vim/rc/general-settings.vim \
  && echo "set smarttab expandtab smartcase ignorecase noswapfile linebreak" >> ~/.vim/rc/general-settings.vim \
  && echo "set hlsearch showmatch matchtime=2 timeout nolazyredraw tabstop=2" >> ~/.vim/rc/general-settings.vim \
  && echo "set laststatus=2 noshowcmd showmode" >> ~/.vim/rc/general-settings.vim \
  && echo "set statusline=\ %F\ %m" >> ~/.vim/rc/general-settings.vim \
  && echo "autocmd TermOpen * setlocal nonumber norelativenumber" >> ~/.vim/rc/general-settings.vim \
  && echo "set completeopt=noinsert,noselect,menuone" >> ~/.vim/rc/general-settings.vim \
  && echo "set nofoldenable termguicolors autoindent smartindent novisualbell noerrorbells" >> ~/.vim/rc/general-settings.vim \
  # Mappings
  && echo 'let mapleader=","' >> ~/.vim/rc/mappings.vim \
  && echo 'let g:mapleader=","' >> ~/.vim/rc/mappings.vim \
  && echo 'let maplocalleader=","' >> ~/.vim/rc/mappings.vim \
  && echo 'let g:maplocalleader=","' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> ] :silent bn<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> } :silent bp<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <space> :nohlsearch<CR>' >> ~/.vim/rc/mappings.vim \
  && echo "nnoremap ' X" >> ~/.vim/rc/mappings.vim \
  && echo 'noremap j gj' >> ~/.vim/rc/mappings.vim \
  && echo 'noremap k gk' >> ~/.vim/rc/mappings.vim \
  && echo 'noremap ; :' >> ~/.vim/rc/mappings.vim \
  && echo 'noremap : ;' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <Leader>v<CR> :e ~/.vimrc<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>H :vertical res -8<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>J :res -8<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>K :res +8<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>L :vertical res +8<CR>' >> ~/.vim/rc/mappings.vim \
  # && echo 'nnoremap <silent> <Leader>h :wincmd h<CR>' >> ~/.vim/rc/mappings.vim \
  # && echo 'nnoremap <silent> <Leader>j :wincmd j<CR>' >> ~/.vim/rc/mappings.vim \
  # && echo 'nnoremap <silent> <Leader>k :wincmd k<CR>' >> ~/.vim/rc/mappings.vim \
  # && echo 'nnoremap <silent> <Leader>l :wincmd l<CR>' >> ~/.vim/rc/mappings.vim \
  # && echo 'nnoremap <silent> <Leader>O <C-w>K' >> ~/.vim/rc/mappings.vim \
  # && echo 'nnoremap <silent> <Leader>o <C-w>H' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>q<CR> :bdelete %<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader><Leader><Leader>q<CR> :bdelete! %<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>w :lclose<CR>:cclose<CR>:pclose<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>4<CR> :so ~/.vimrc<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <Leader>t2 :set shiftwidth=2<CR>:set tabstop=2<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <Leader>t4 :set shiftwidth=4<CR>:set tabstop=4<CR>' >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>; :set cursorline!<CR>' >> ~/.vim/rc/mappings.vim \
  && echo "nnoremap <silent> <Leader>' :set number!<CR>" >> ~/.vim/rc/mappings.vim \
  && echo 'nnoremap <silent> <Leader>" :set relativenumber!<CR>' >> ~/.vim/rc/mappings.vim \
  # plugins
  && echo "call plug#begin('~/.vim/plugged')" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'MunifTanjim/nui.nvim'," >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'akinsho/org-bullets.nvim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'antoinemadec/coc-fzf', { 'branch': 'release' }" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'neoclide/coc.nvim', {'branch': 'release'}" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'folke/tokyonight.nvim'" >> ~/.vim/rc/plugins.vim \
  # && echo "Plug 'folke/which-key.nvim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'hrsh7th/nvim-cmp'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin >/dev/null' }" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'junegunn/fzf.vim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'junegunn/goyo.vim'" >> ~/.vim/rc/plugins.vim \
  # && echo "Plug 'lukas-reineke/indent-blankline.nvim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'machakann/vim-highlightedyank'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'martineausimon/nvim-lilypond-suite'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'metakirby5/codi.vim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'neovim/nvim-lspconfig'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'nvim-lua/plenary.nvim'," >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'nvim-orgmode/orgmode'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'nvim-telescope/telescope-file-browser.nvim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'nvim-telescope/telescope.nvim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }" >> ~/.vim/rc/plugins.vim \
  # && echo "Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate | TSInstall python bash typescript' }" >> ~/.vim/rc/plugins \
  && echo "Plug 'rhysd/clever-f.vim'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'rust-lang/rust.vim', { 'for': 'rust' }" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'tpope/vim-commentary'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'tpope/vim-dispatch'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'tpope/vim-repeat'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'tpope/vim-sleuth'" >> ~/.vim/rc/plugins.vim \
  && echo "Plug 'tpope/vim-surround'" >> ~/.vim/rc/plugins.vim \
  # && echo "Plug 'w0rp/ale'" >> ~/.vim/rc/plugins.vim \
  && echo "call plug#end()" >> ~/.vim/rc/plugins.vim

# Install plugins
RUN nvim -V1 -Es -n -i NONE -u ~/.config/nvim/init.vim +'PlugInstall' +'qa' 2>&1 \
 && echo "colorscheme tokyonight-moon" >> ~/.vimrc

# Install coc extensions
RUN mkdir -pv /home/devuser/.config/coc \
  && mkdir -pv /home/devuser/.config/coc/extensions \
  && echo '{"dependencies":{}}' > /home/devuser/.config/coc/extensions/package.json \
  && cd /home/devuser/.config/coc/extensions \
  # Everything but coc-pyright goes here because this pyright does its own postinstall logic
  && npm install coc-json coc-docker coc-prettier 2>&1

# Pandora
# RUN sudo apt-get update && sudo apt-get install -y --quiet --no-install-recommends pianobar

# ---------------- Dotfiles / Config / NOT Packages ------------------

# Allow ranger to open selected file(s)
ENV EDITOR=/usr/local/bin/nvim

# Plugin Config
RUN echo "let g:fzf_layout = { 'window': { 'width': 0.95, 'height': 0.7, 'yoffset': 0.6 } }" >> ~/.vim/rc/plugins.vim \
  && echo "let g:coc_fzf_preview='down:70%'" >> ~/.vim/rc/plugins.vim \
  # Install on first nvim open, fixes coc-pyright needing some postinstall action in order to work
  && echo "let g:coc_global_extensions = ['coc-pyright', 'coc-tsserver']" >> ~/.vim/rc/plugins.vim \
  && echo "let g:highlightedyank_highlight_duration = 500" >> ~/.vim/rc/plugins.vim \
  && echo "" >> ~/.vim/rc/plugins.vim

# Lua Plugin settings
RUN echo "require('nvls').setup({})" >> ~/.vim/rc/plugin-settings.lua \
  && echo "require('org-bullets').setup()" >> ~/.vim/rc/plugin-settings.lua \
  && echo "require('cmp').setup({ sources = { { name = 'orgmode' } } })" >> ~/.vim/rc/plugin-settings.lua \
  # && echo "require('which-key').setup({})" >> ~/.vim/rc/plugin-settings.lua \
  # && echo "require('ibl').setup({ scope = { show_start = false, show_end = false } })" >> ~/.vim/rc/plugin-settings.lua \
  && echo '\n\
  require("telescope").setup {\n\
  defaults = {\n\
  layout_config = {\n\
  height = 0.7,\n\
  },\n\
  },\n\
  extensions = {\n\
  file_browser = { hidden = true },\n\
  hijack_netrw = true\n\
  }\n\
  }\n\
  \n\
  require("telescope").load_extension "file_browser"\n\
  \n\
  local orgmode = require("orgmode")\n\
  orgmode.setup_ts_grammar()\n\
  orgmode.setup({\n\
  org_agenda_files = {\n\
  "~/org/**/*",\n\
  },\n\
  org_default_notes_file = "~/host-fs/org/refile.org",\n\
  org_capture_templates = {\n\
  j = {\n\
  description = "Journal",\n\
  template = "\\\\n* %<%A> Journal %U\\\\n\\\\n%?",\n\
  target = "~/host-fs/org/journal.org",\n\
  },\n\
  require("nvim-treesitter.configs").setup {\n\
  highlight = {\n\
  enable = true,\n\
  additional_vim_regex_highlighting = { "org" },\n\
  },\n\
  indent = { enable = false },\n\
  ensure_installed = { "org", "bash", "typescript", "python" }\n\
  }\n\
  }\n\
  })\n\
  ' >> ~/.vim/rc/plugin-settings.lua \
  # Plugin Mappings
  && echo "nnoremap <silent> <C-e> :Goyo 110x95%<CR> " >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <silent> <C-e><C-e> :Goyo!<CR> " >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <silent> \ :Telescope file_browser<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <C-p> :Telescope git_files<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <C-b> :Telescope buffers<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <C-h> :Telescope oldfiles<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <C-v> :Telescope commands<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <C-t> :Telescope<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <C-j> :Telescope keymaps<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <Leader><C-p> :Telescope find_files<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo "nnoremap <Leader><C-r> :Telescope live_grep<CR>" >> ~/.vim/rc/plugin-mappings.vim \
  && echo 'nnoremap <silent> <Leader>n <Plug>(coc-diagnostic-next-error)' >> ~/.vim/rc/plugin-mappings.vim \
  && echo 'nnoremap <silent> <Leader>p <Plug>(coc-diagnostic-prev-error)' >> ~/.vim/rc/plugin-mappings.vim \
  && echo 'nnoremap <silent> <C-f> <Plug>(coc-format)' >> ~/.vim/rc/plugin-mappings.vim \
  && echo 'nmap <silent> <Leader>ac <Plug>(coc-codeaction-selected)w' >> ~/.vim/rc/plugin-mappings.vim \
  && echo 'nnoremap <silent> <Leader>d <Plug>(coc-definition)' >> ~/.vim/rc/plugin-mappings.vim \
  && echo 'nnoremap <silent> <Leader>r <Plug>(coc-references)' >> ~/.vim/rc/plugin-mappings.vim \
  && echo 'nnoremap <Leader>ly :w<CR>:Start! lilypond -o %:p:h %:p<CR>:Start! mupdf-gl %:p:r.pdf<CR>' >> ~/.vim/rc/mappings.vim \
  && echo "" >> ~/.vim/rc/plugin-mappings.vim 

# Prompt String
RUN echo "txtblk='\\e[0;30m'" >> ~/.bashrc \
  && echo "txtwht='\\e[0;37m'" >> ~/.bashrc \
  && echo "txtylw='\\e[0;33m'" >> ~/.bashrc \
  && echo "txtred='\\e[0;31m'" >> ~/.bashrc \
  && echo "txtgrn='\\e[0;32m'" >> ~/.bashrc \
  && echo "txtpur='\\e[0;35m'" >> ~/.bashrc \
  && echo "txtblu='\\e[0;34m'" >> ~/.bashrc \
  && echo "txtcyn='\\e[0;36m'" >> ~/.bashrc \
  && echo "bakblk='\\e[40m'" >> ~/.bashrc \
  && echo "arr=(\$txtwht \$txtylw \$txtred \$txtgrn \$txtpur \$txtblu \$txtcyn)" >> ~/.bashrc \
  && echo "txt=\${arr[\$RANDOM % \${#arr[@]}]}" >> ~/.bashrc \
  && echo 'export PS1="\\n\\$txt\\$bakblk   DEBIAN  \\w    \\$txtblk\\$reset "' >> ~/.bashrc \
  # Aliases
  && echo 'alias t="clear ; tmux attach -t All &>/dev/null || tmux new -s All"' >> ~/.bashrc \
  && echo 'alias tt="clear ; tmux attach -t Temp &>/dev/null || tmux new -s Temp"' >> ~/.bashrc \
  # && echo "if [[ -z "\$TMUX" ]]; then t; fi" >> ~/.bashrc \
  && echo 'alias q="exit"' >> ~/.bashrc \
  && echo 'alias r="ranger"' >> ~/.bashrc \
  && echo 'alias gs="git status"' >> ~/.bashrc \
  && echo 'alias ..="cd .."' >> ~/.bashrc \
  && echo 'alias ...="cd ../.."' >> ~/.bashrc \
  && echo 'alias ....="cd ../../.."' >> ~/.bashrc \
  && echo 'alias pbcopy="xclip -selection clipboard"' >> ~/.bashrc \
  && echo 'alias pbpaste="xclip -selection clipboard -o"' >> ~/.bashrc \
  && echo 'alias tkill="tmux kill-pane -t $@"' >> ~/.bashrc 

# Ranger config
RUN mkdir -pv /home/devuser/.config/ranger/ \
  && echo "set column_ratios 1,4" >> /home/devuser/.config/ranger/rc.conf

# align Neovim background with system/host theme
RUN echo "let &background = \$HOST_SYSTEM_THEME" >> ~/.vimrc 

# Tmux configuration
RUN echo "set-option -g prefix C-j" >> ~/.tmux.conf \
  && echo "unbind C-b" >> ~/.tmux.conf \
  && echo 'bind r source-file ~/.tmux.conf \; display "Reloaded!"' >> ~/.tmux.conf \
  && echo "bind-key h select-pane -L" >> ~/.tmux.conf \
  && echo "bind-key j select-pane -D" >> ~/.tmux.conf \
  && echo "bind-key k select-pane -U" >> ~/.tmux.conf \
  && echo "bind-key l select-pane -R" >> ~/.tmux.conf \
  && echo "bind-key -r H resize-pane -L 8" >> ~/.tmux.conf \
  && echo "bind-key -r J resize-pane -D 8" >> ~/.tmux.conf \
  && echo "bind-key -r K resize-pane -U 8" >> ~/.tmux.conf \
  && echo "bind-key -r L resize-pane -R 8" >> ~/.tmux.conf \
  && echo "bind o split-window -h" >> ~/.tmux.conf \
  && echo "bind i split-window -v" >> ~/.tmux.conf \
  && echo "bind O select-layout even-horizontal" >> ~/.tmux.conf \
  && echo "bind I select-layout even-vertical" >> ~/.tmux.conf \
  && echo "bind b break-pane" >> ~/.tmux.conf \
  && echo "setw -g aggressive-resize on" >> ~/.tmux.conf \
  && echo "set -g mouse on" >> ~/.tmux.conf \
  && echo "set-window-option -g mode-keys vi" >> ~/.tmux.conf \
  && echo "set -g status-interval 8" >> ~/.tmux.conf \
  # && echo "set -g base-index 1" >> ~/.tmux.conf \
  # && echo "setw -g pane-base-index 1" >> ~/.tmux.conf \
  # tmuxline conf
  && echo "set -gq allow-passthrough on" >> ~/.tmux.conf \
  && echo 'set -g status-position "bottom"' >> ~/.tmux.conf \
  && echo 'set -g pane-active-border-style bg=0' >> ~/.tmux.conf \
  && echo 'set -g pane-active-border-style fg=4' >> ~/.tmux.conf \
  && echo 'set -g pane-border-style bg=4' >> ~/.tmux.conf \
  && echo 'set -g pane-border-style fg=0' >> ~/.tmux.conf \
  && echo 'set -g status-style fg=colour15 ' >> ~/.tmux.conf \
  && echo 'set -g status-style bg=0' >> ~/.tmux.conf \
  && echo 'set -g default-terminal "tmux-256color"' >> ~/.tmux.conf \
  # && echo 'set -g status-left " #(tmux list-windows)"' >> ~/.tmux.conf \
  && echo '' >> ~/.tmux.conf 

ENV LC_ALL en_US.UTF-8

# Move to base directory
WORKDIR /home/devuser

# Start in bash
CMD ["/bin/bash"]
