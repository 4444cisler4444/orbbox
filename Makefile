REPOSITORY := cisler/orbbox

# all: dotfiles
all: base

# socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"192.168.56.170:0\" ; \
# xhost +SI:localuser:$$USER ; \

run:
	@export hostSystemTheme=`defaults read -g AppleInterfaceStyle &>/dev/null && echo dark || echo light` ; \
	export kittenTheme=`defaults read -g AppleInterfaceStyle &>/dev/null && echo Moon || echo Day` ; \
	kitten themes --reload-in=all Tokyo Night $$kittenTheme ; \
	docker run -it --rm --tty \
		-v "${HOME}/.ssh:/home/devuser/.ssh:ro" \
		-v "${HOME}:/home/devuser/host-fs" \
		--net=host \
		--cpus 2 \
		--memory 2gb \
		--env DISPLAY=192.168.56.170:0 \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		--env HOST_SYSTEM_THEME=$$hostSystemTheme \
		--env "LEDGER_FILE=/home/devuser/host-fs/.hledger.journal" \
		--name orbbox-base \
		$(REPOSITORY)-base

# clean-base:
# 	@docker rmi --force $(REPOSITORY)-base

clean:
	@docker rmi --force $(REPOSITORY)-base

base:
	@docker build --tag $(REPOSITORY)-base .

# dotfiles: base
# 	@docker build --no-cache --tag $(REPOSITORY) .

# cached: base
# 	@docker build --tag $(REPOSITORY) .

.PHONY: all base   
# .PHONY: all base dotfiles clean cached
